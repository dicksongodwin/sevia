from django.db import connections


def dictfetchall(cursor):
    "Return all rows from a cursor as a dict"
    rows_affected = cursor.rowcount
    print('rows affected:', rows_affected)
    if not cursor.description: return []
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def execute(query, params=[], connection="default"):
    with connections[connection].cursor() as cursor:
        cursor.execute(query, params)
        return dictfetchall(cursor)


def fetch_one(query, params=[], connection='default'):
    res = execute(query, params=params, connection=connection)
    if res and len(res) > 0: return res[0]
    else: return None