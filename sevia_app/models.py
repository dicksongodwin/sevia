from django.db import models
from accounts.models import User
import datetime
from django.utils.translation import ugettext_lazy as _
import uuid
from django.contrib.auth.models import Group
from django.contrib.postgres.fields import JSONField


ASSESSMENT_CHOICES = [
        ('VIA neg','VIA Negative No treatment'),
        ('VIA+ for cryotherapy','VIA Positive for cryotherapy/Thermal Cagulation'),
        ('VIA+ LEEP','VIA Positive LEEP'),
        ('VIA+ refer LEEP','VIA Positive Refer ForLEEP'),
        ('Suspect Cancer','Suspect For Cancer Take Punch Biopsy'),
        ('Cervicitis Give Antibiotics','Cervicitis Give Antibiotics'),
        ('Cervical Polyp','Cervical Polyp (Refer / Do Polypectomy)'),
        ('Others','Others')
        ]

class Country(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30, blank=True)

    class Meta:
        verbose_name_plural = "Countries"
    
    def __str__(self):
        return self.name

class Region(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30, blank=True)
    country = models.ForeignKey(Country,on_delete=models.PROTECT,)

    
    def __str__(self):
        return self.name

class District(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=30, blank=True)
    region = models.ForeignKey(Region,on_delete=models.PROTECT,)

    
    def __str__(self):
        return self.name

class FacilityGroupDetail(models.Model):
    id = models.AutoField(primary_key=True)
    group_name = models.CharField(max_length=100, blank=True)
    users = models.ManyToManyField(User)
    def __str__(self):
        return self.group_name

class Facility(models.Model):
    id = models.AutoField(primary_key=True)
    code = models.CharField(max_length=30, blank=True,null=True)
    district = models.ForeignKey(District,on_delete=models.PROTECT,)
    facility_group = models.ForeignKey(FacilityGroupDetail,on_delete=models.PROTECT,)
    name = models.CharField(max_length=60, blank=True)
    phone = models.CharField(max_length=60, null=True,blank=True)
    users = models.ManyToManyField(User)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,null=True)

    class Meta:
        verbose_name_plural = "Facilities"
    
    def __str__(self):
        return self.name

    def get_full_address(self):
        return self.district.name + ' , ' + self.district.region.name



class Form(models.Model):
    id = models.AutoField(primary_key=True)
    content = models.TextField(null=True,blank=True,default=None)
    country_id = models.ForeignKey(Country, on_delete=models.PROTECT,null=True,blank=True)
    group = models.ForeignKey(Group,on_delete=models.PROTECT,null=True,blank=True,default=None)
    table_name = models.CharField(max_length=30)
    table_def = models.TextField(null=True,blank=True,default=None)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.table_name

class FormData(models.Model):
    uri = models.CharField(primary_key=True,max_length=64,default=uuid.uuid4)
    form_id = models.ForeignKey(Form,on_delete=models.PROTECT,)
    created_by = models.ForeignKey(User,on_delete=models.PROTECT,)
    exported = models.BooleanField(default=0)
    exported_at = models.DateTimeField(blank=True,default=None,null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    data = JSONField(null=True)

    def __str__(self):
        return self.uri

class Patient(models.Model):
    id = models.AutoField(primary_key=True)
    first_name = models.CharField(verbose_name='First Name',max_length=30,blank=True,null=True,default=None)
    middle_name = models.CharField(verbose_name='Middle Name',max_length=30,blank=True,null=True,default=None)
    last_name = models.CharField(verbose_name='Sur Name',max_length=30,blank=True,null=True,default=None)
    phone = models.CharField(verbose_name='phonenumber',max_length=30,null=True, blank=True)
    age = models.CharField(verbose_name='age',max_length=30,null=True, blank=True)
    facility = models.ManyToManyField(Facility)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True,null=True, blank=True)

    def __str__(self):
        return str(self.id)

    def get_full_name(self):
        first_name = self.first_name if self.first_name else '' 
        middle_name = self.middle_name if self.middle_name else '' 
        last_name = self.last_name if self.last_name else '' 
        fullname = self.first_name + ' ' + middle_name +' ' + last_name
        return str(fullname)


class FormPatientDataV1(models.Model):

    PITC_CHOICES = [('No','No'),('Yes','Yes')]
    HIV_CHOICES = [('Positive','Positive'),('Negative','Negative'),('Unknown','Unknown')]
    VISIT_CHOICES = [
        ('New client (nevwer been screened)','New client (nevwer been screened)'),
        ('Postponed cryotherapy','Postponed cryotherapy'),
        ('Post-treatment complication (Cryo or LEEP)','Post-treatment complication (Cryo or LEEP)'),
        ('LEEP','LEEP'),
        ('1 year follow-up VIA+ (VIA+ treated with Cryo or LEEP)','1 year follow-up VIA+ (VIA+ treated with Cryo or LEEP)'),
        ('Routine VIA screening (returning client previously VIA-)','Routine VIA screening (returning client previously VIA-)'),
        ('Referred for VIA+ client(reffered for cryo)','Referred for VIA+ client(reffered for cryo)'),
        ('Referred for suspect cancer','Referred for suspect cancer'),
        ('Post-cryotherapy images','Post-cryotherapy images'),
        ]
    

    uri = models.CharField(max_length=64,primary_key=True,unique=True,default=uuid.uuid4)
    patient = models.ForeignKey(Patient,on_delete=models.PROTECT,)
    provider = models.ForeignKey(User,on_delete=models.PROTECT,related_name="provider")
    ccs_providers_full_name = models.CharField(max_length=128,blank=True, null=True)
    ccs_providers_phone_no = models.CharField(max_length=128,blank=True, null=True)
    screen_date = models.DateField(default=datetime.date.today)
    parity = models.BigIntegerField(blank=True, null=True)
    patient_received_pitc = models.CharField(max_length=128,default=None,blank=True, null=True,choices=PITC_CHOICES,)
    ccs_facility_no = models.CharField(max_length=128,blank=True, null=True)
    ccs_no = models.CharField(max_length=128,blank=True, null=True)
    hiv_status = models.CharField(max_length=128,default=None,blank=True, null=True,choices=HIV_CHOICES,)
    assessment = models.CharField(max_length=128,default=None,blank=True, null=True,choices=ASSESSMENT_CHOICES,)
    visit_status = models.CharField(max_length=128,default=None,blank=True, null=True, choices=VISIT_CHOICES,)
    photo_1 = models.ImageField(upload_to='patient_data/%Y/%m/%d',blank=True, null=True)
    photo_2 = models.ImageField(upload_to='patient_data/%Y/%m/%d', blank=True, null=True)
    photo_3 = models.ImageField(upload_to='patient_data/%Y/%m/%d', blank=True, null=True)
    form_version = models.CharField(max_length=128,blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.uri)

    class Meta:
        # managed = False
        db_table = '_form_patient_data_v1'

class FormReviewerDataV1(models.Model):
    uri = models.CharField(max_length=64,primary_key=True,default=uuid.uuid4)
    patient_data = models.OneToOneField(FormPatientDataV1, to_field="uri",on_delete=models.PROTECT,related_name="reviewer_data",db_constraint=False)
    created_at = models.DateTimeField(auto_now_add=True)
    created_by = models.ForeignKey(User,on_delete=models.PROTECT,related_name="creator",db_constraint=False)
    assessment = models.CharField(max_length=128, blank=True, null=True,choices=ASSESSMENT_CHOICES,)
    comment = models.CharField(max_length=128, blank=True, null=True)
    mismatch = models.IntegerField(blank=True, null=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)


    def __str__(self):
        return str(self.uri)
        
    class Meta:
        # managed = False
        db_table = '_form_reviewer_data_v1'


class DataDictionary(models.Model):
    formId = models.TextField(null=True,blank=True,default=None)
    formVersion = models.TextField(null=True,blank=True,default=None)
    variableKey= models.TextField(null=True,blank=True,default=None)
    variableName = models.TextField(null=True,blank=True,default=None)
    description = models.TextField(null=True,blank=True,default=None)
    status = models.IntegerField(blank=True, null=True,default=0)

    
    def __str__(self):
        return self.formId

class FormPatientDataV2(models.Model):

    HIV_CHOICES = [('Positive','Positive'),('Negative','Negative'),('Unknown','Unknown')]
    
    uri = models.CharField(max_length=64,primary_key=True,unique=True,default=uuid.uuid4)
    patient = models.ForeignKey(Patient,on_delete=models.PROTECT,)
    provider = models.ForeignKey(User,on_delete=models.PROTECT)
    visit_date = models.DateField(default=datetime.date.today)
    visit_status = models.CharField(max_length=128,default=None,blank=True, null=True)
    first_initial = models.CharField(max_length=30,blank=True,null=True,default=None)
    middle_initial = models.CharField(max_length=30,blank=True,null=True,default=None)
    last_initial = models.CharField(max_length=30,blank=True,null=True,default=None)
    phone = models.CharField(max_length=30,blank=True,null=True,default=None)
    age = models.IntegerField(blank=True, null=True)
    passport = models.CharField(max_length=30,blank=True,null=True,default=None)
    followup_reasons = models.CharField(max_length=128,default=None,blank=True, null=True)
    treatment_comp = models.CharField(max_length=128,default=None,blank=True, null=True)
    parity = models.BigIntegerField(blank=True, null=True)
    hpv_dna_results = models.CharField(max_length=128,default=None,blank=True, null=True)
    dna_types = models.CharField(max_length=128,default=None,blank=True, null=True)
    hiv_status = models.CharField(max_length=128,default=None,blank=True, null=True,choices=HIV_CHOICES,)
    via_status = models.CharField(max_length=128,default=None,blank=True, null=True)
    image = models.ImageField(upload_to='patient_data/%Y/%m/%d',blank=True, null=True)
    assessment = models.CharField(max_length=128,default=None,blank=True, null=True,choices=ASSESSMENT_CHOICES,)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, null=True)

    def __str__(self):
        return str(self.uri)

    class Meta:
        # managed = False
        db_table = '_form_patient_data_v2'
