INSERT INTO auth_group (name) VALUES
	 ('CCS Provider'),
	 ('CCS Image Reviewer'),
	 ('System Administrator'),
	 ('SEVIA Admin'),
	 ('CCS Trainer'),
	 ('Program Manager');