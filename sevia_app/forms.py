from django import forms
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.admin.widgets import FilteredSelectMultiple    
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _
from sevia_app.models import FormPatientDataV2,FormReviewerDataV1

User = get_user_model()


class FormPatientDataCreationForm(forms.ModelForm):
    screen_date = forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date'}))

    def __init__(self, *args, **kargs):
        super(FormPatientDataCreationForm, self).__init__(*args, **kargs)
        
    class Meta:
        model = FormPatientDataV2
        exclude = ('provider','uri')

    def save(self, commit=True):
        formdata = super(FormPatientDataCreationForm, self).save(commit=False)
        if commit:
            formdata.save()
        return formdata


class FormPatientDataChangeForm(forms.ModelForm):
    screen_date = forms.DateField(widget=forms.widgets.DateInput(attrs={'type': 'date'}))

    def __init__(self, *args, **kargs):
        super(FormPatientDataChangeForm, self).__init__(*args, **kargs)

    class Meta:
        model = FormPatientDataV2
        exclude = ('provider','uri')

class FormReviewerDataCreationForm(forms.ModelForm):

    def __init__(self, *args, **kargs):
        super(FormReviewerDataCreationForm, self).__init__(*args, **kargs)
        
    class Meta:
        model = FormReviewerDataV1
        exclude = ('patient_data','created_by','uri','mismatch')




