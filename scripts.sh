# installing django and virtual env
# sudo apt install python3-pip python3-django

sudo apt install python3-venv

# creating virtual env
# go tothe working dir and execute the following
python3 -m venv env
source env/bin/activate

# install dependences
pip install django
pip install django-crispy-forms
pip install djangorestframework
pip install markdown 

