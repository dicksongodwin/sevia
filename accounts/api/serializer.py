from accounts.models import User
from rest_framework import serializers
from django.contrib.auth.models import Group


class GroupSerializer(serializers.ModelSerializer):    
    class Meta:
        model = Group
        fields = ('id','name',)

class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True,read_only=True,)

    class Meta:
        model = User
        fields = ['username', 'email','fcm_token', 'fullname','staff','admin','active','groups']


