from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser,PermissionsMixin
)

class UserManager(BaseUserManager):

    def get_by_natural_key(self, username):
        return self.get(username__iexact=username) #caseinsensitive username 

    def create_user(self, username,fullname, password=None,**extra_fields):
        """
        Creates and saves a User with the given username and password.
        """
        if not username:
            raise ValueError('Users must have a username')

        if not fullname:
            raise ValueError('Users must have a name')

        user = self.model(
            username=username,
            fullname=fullname, 
            **extra_fields
            )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_staffuser(self,username,fullname, password,**extra_fields):
        """
        Creates and saves a staff user with the given email and password.
        """
        user = self.create_user(
            username=username,
            fullname=fullname,
            password=password,
        )
        user.staff = True
        user.save(using=self._db)
        return user

    def create_superuser(self, username,fullname,password,**extra_fields):
        """
        Creates and saves a superuser with the given email and password.
        """
        user = self.create_user(
            username=username,
            fullname=fullname,
            password=password,
        )
        user.staff = True
        user.admin = True
        user.is_superuser = True
        user.save(using=self._db)
        return user

class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(verbose_name='email address',max_length=64,blank=True,null=True)
    username = models.CharField(verbose_name='username',max_length=30,unique=True)
    fullname = models.CharField(verbose_name='name',max_length=30)
    phone = models.CharField(verbose_name='phonenumber',max_length=30, blank=True,null=True)
    fcm_token = models.CharField(verbose_name='fcm token',max_length=152, blank=True,null=True)
    active = models.BooleanField(default=True)
    staff = models.BooleanField(default=False) # a admin user; non super-user
    admin = models.BooleanField(default=False) # a superuser
    created_on = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    # notice the absence of a "Password field", that is built in.

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['fullname'] 

    objects = UserManager()

    def get_full_name(self):
        # The user is identified by their email address
        return self.fullname

    def get_short_name(self):
        # The user is identified by their email address
        return self.fullname

    def __str__(self):              # __unicode__ on Python 2
        return self.fullname

    # def has_perm(self, perm, obj=None):
    #     "Does the user have a specific permission?"
    #     # Simplest possible answer: Yes, always
    #     return True

    # def has_module_perms(self, app_label):
    #     "Does the user have permissions to view the app `app_label`?"
    #     # Simplest possible answer: Yes, always
    #     return True
    def get_total_cases(self):
        # get tottal number of cases the user is associated as provider and as reviewer
        return self.formpatientdatav2_set.count() + self.creator.count()    
    @property
    def is_staff(self):
        "Is the user a member of staff?"
        return self.staff

    @property
    def is_admin(self):
        "Is the user a admin member?"
        return self.admin

    @property
    def is_active(self):
        "Is the user active?"
        return self.active
