from random import choices
from django import forms, VERSION
from django.contrib.auth.forms import ReadOnlyPasswordHashField
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.admin.widgets import FilteredSelectMultiple    
from django.contrib.auth.models import Group
from django.utils.translation import ugettext_lazy as _
from sevia_app.models import Facility

User = get_user_model()


def validate_digits_letters(word):
    for char in word:
        if char == '_' or char == '.' : #allowed special char
            return True
        if not char.isdigit() and not char.isalpha():
            return False
    return True

class LoginForm(forms.Form):  
    username = forms.CharField(widget=forms.TextInput(attrs={'placeholder':'Username'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder':'Password'}))

class UserAdminCreationForm(forms.ModelForm):
    
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat Password', widget=forms.PasswordInput)

    def __init__(self, *args, **kargs):
        super(UserAdminCreationForm, self).__init__(*args, **kargs)

    class Meta:
        model = User
        fields = ("username","fullname","phone")

    def clean_username(self):
        data = self.cleaned_data['username']
        data = data.lower()
        unique_username = User.object.filter(username__iexact=data).first()

        if unique_username:
            raise forms.ValidationError("Usernames should be unique")

        if not data.islower():
            raise forms.ValidationError("Usernames should be in lowercase")

        if not validate_digits_letters(data):
            raise forms.ValidationError("Usernames contains characters that are not numbers nor letters")

        return data

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Passwords don\'t match')
        return password2

    def save(self, commit=True):
        user = super(UserAdminCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password2'])
        if commit:
            user.save()
        return user


class UserAdminChangeForm(forms.ModelForm):
    
    def __init__(self, *args, **kargs):
        super(UserAdminChangeForm, self).__init__(*args, **kargs)


    password = ReadOnlyPasswordHashField(widget=forms.HiddenInput())

    class Meta:
        model = User
        fields = ('username','fullname','phone','email','fcm_token', 'password', 'active', 'admin','staff')

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

class CustomeUserCreationForm(forms.ModelForm):
    
    roles = forms.ModelChoiceField(queryset=Group.objects.all())
    facility = forms.MultipleChoiceField(choices=Facility.objects.values_list('id', 'name'))
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat Password', widget=forms.PasswordInput)

    def __init__(self, *args, **kwargs):
        user = kwargs.pop('user')
        super(CustomeUserCreationForm, self).__init__(*args, **kwargs)
        if not user.is_admin:
            self.fields['facility'] = forms.MultipleChoiceField(choices=Facility.objects.values_list('id', 'name'))
            self.fields['roles'] = forms.ModelChoiceField(queryset=Group.objects.filter(name__in=['CCS Image Reviewer','CCS Provider']).all())


    class Meta:
        model = User
        fields = ("username","fullname",'email',"phone")


    def clean_username(self):
        data = self.cleaned_data['username']
        data = data.lower()
        if not data.islower():
            raise forms.ValidationError("Usernames should be in lowercase")

        if not validate_digits_letters(data):
            raise forms.ValidationError("Usernames contains characters that are not numbers nor letters")

        return data

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Passwords don\'t match')
        return password2

    def save(self, commit=True):
        user = super(CustomeUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password2'])
        user.active = False
        if commit:
            user.save()
        user.groups.add(self.cleaned_data['roles'])
        for id in self.cleaned_data['facility']:
            facility  = Facility.objects.get(id=id)
            user.facility_set.add(facility)
        return user

class CustomeUserRegistrationForm(forms.ModelForm):
    
    password1 = forms.CharField(label='Password', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Repeat Password', widget=forms.PasswordInput)

    def __init__(self, *args, **kargs):
        super(CustomeUserRegistrationForm, self).__init__(*args, **kargs)

    class Meta:
        model = User
        fields = ("username","fullname",'email',"phone")

    def clean_username(self):
        data = self.cleaned_data['username']
        data = data.lower()
        if not data.islower():
            raise forms.ValidationError("Usernames should be in lowercase")

        if not validate_digits_letters(data):
            raise forms.ValidationError("Usernames contains characters that are not numbers nor letters")

        return data

    def clean_password2(self):
        password1 = self.cleaned_data.get('password1')
        password2 = self.cleaned_data.get('password2')

        if password1 and password2 and password1 != password2:
            raise forms.ValidationError('Passwords don\'t match')
        return password2

    def save(self, commit=True):
        user = super(CustomeUserRegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password2'])
        user.active = False
        if commit:
            user.save()
        return user

class CustomeUserChangeForm(forms.ModelForm):
    
    groups = forms.ModelChoiceField(queryset=Group.objects.all(),required=False)
    facility = forms.ModelChoiceField(queryset=Facility.objects.all(),required=False)

    def __init__(self, *args, **kargs):
        super(CustomeUserChangeForm, self).__init__(*args, **kargs)
        # self.fields['groups'] = Group.objects.values()

    password = ReadOnlyPasswordHashField(widget=forms.HiddenInput())

    class Meta:
        model = User
        fields = ('username','fullname','phone','email','fcm_token', 'password', 'active','staff')

    def clean_username(self):
        data = self.cleaned_data['username']
        data = data.lower()
        if not data.islower():
            raise forms.ValidationError("Usernames should be in lowercase")

        if not validate_digits_letters(data):
            raise forms.ValidationError("Usernames contains characters that are not numbers nor letters")

        return data

    def clean_password(self):
        # Regardless of what the user provides, return the initial value.
        # This is done here, rather than on the field, because the
        # field does not have access to the initial value
        return self.initial["password"]

    def save(self, commit=True):
        user = super(CustomeUserChangeForm, self).save(commit=False)
        # user.set_password(self.cleaned_data['password'])
        
        if commit:
            user.save()
        if self.cleaned_data['groups']:
            user.groups.clear() #remove old groups
            user.groups.add(self.cleaned_data['groups'])

        if self.cleaned_data['facility']:
            user.facility_set.clear() #remove old facility
            user.facility_set.add(self.cleaned_data['facility'])
        return user