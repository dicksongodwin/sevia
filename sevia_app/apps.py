from django.apps import AppConfig


class SeviaAppConfig(AppConfig):
    name = 'sevia_app'
