import time
import json
import requests

def send_to_rabbit(message_as_dict: dict,
                   exchange: str,
                   routing_key: str,
                   headers: dict):
    url = "http://45.55.80.226:15672/api/exchanges/%2F/sevia/publish"

    payload = {
        "vhost": "/",
        "name": "sevia",
        "properties": {
            "delivery_mode": 2,
            "content-type": "application/json",
            "headers": headers
        },
        "routing_key": "case_received",
        "delivery_mode": "2",
        "payload": json.dumps(message_as_dict),
        "props": {},
        "payload_encoding": "string"
    }
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Basic ZnJhbms6azBpNTEyajBpMjJpMw=="
    }

    response = requests.request("POST", url, json=payload, headers=headers)
    return response





def get_rebus_headers():
    """
    rbs2-content-type:	application/json;charset=utf-8
    rbs2-corr-id:	a64e38c1-77e7-43dc-9e62-a5a89e55999c
    rbs2-corr-seq:	0
    rbs2-intent:	p2p
    rbs2-msg-id:	a64e38c1-77e7-43dc-9e62-a5a89e55999c
    rbs2-msg-type:	SeviaServices.Messages.MessageEnvelope`1[[SeviaServices.Messages.FormDataMessage, SeviaServices]], SeviaServices
    rbs2-return-address:	sevia_events
    rbs2-sender-address:	sevia_events
    rbs2-senttime:	2022-04-28T19:47:33.8519184+03:00
    """
    case_received_message_type = 'SeviaServices.Messages.MessageEnvelope`1[[SeviaServices.Messages.FormDataMessage, SeviaServices]], SeviaServices'
    rebus_headers = {
            'rbs2-content-type': 'application/json;charset=utf-8',
            'rbs2-return-address':'sevia_events',
            'rbs2-sender-address':'sevia_events',
            'rbs2-msg-type': case_received_message_type
        }
    return rebus_headers


def put_in_envelope(message):
    return {'RetryCounts': 0, 'Message': message}



def do_send_event(data: dict):
    res = send_to_rabbit(put_in_envelope(data), "sevia", "case_received", get_rebus_headers())
    return res


# sample data: {"uri": "test", "formId": "5"}
if __name__ == "__main__": pass


       