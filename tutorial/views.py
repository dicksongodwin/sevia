from django.shortcuts import render
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from tutorial.models import Tutorial

class TutorialListIndexView(LoginRequiredMixin, generic.ListView):
    template_name ='tutorial/tutorial_list_index.html'
    def get_queryset(self):
        return Tutorial.objects.all()

class TutorialDetailView(LoginRequiredMixin, generic.DetailView):
    template_name ='tutorial/tutorial_detail.html'
    context_object_name = 'tutorial'
    queryset = Tutorial.objects.all()

class TutorialListView(LoginRequiredMixin, generic.ListView):
    template_name ='tutorial/tutorial_list.html'
    def get_queryset(self):
        return Tutorial.objects.all()

class TutorialCreateView(LoginRequiredMixin, generic.CreateView):
    template_name ='tutorial/tutorial_create_form.html'
    model = Tutorial
    fields = "__all__"
    success_url = reverse_lazy('tutorial:tutorial_create')

class TutorialUpdateView(LoginRequiredMixin, generic.UpdateView):
    template_name ='tutorial/tutorial_update_form.html'
    model = Tutorial
    fields = "__all__"
    success_url = reverse_lazy('tutorial:tutorial_list')



class TutorialDeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name ='tutorial/tutorial_delete_form.html'
    model = Tutorial
    success_url = reverse_lazy('tutorial:tutorial_list')
