from django.urls import path,include
from accounts.api.views import UserViewSet,GetAuthToken
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'users', UserViewSet)

urlpatterns = [
    path('', include(router.urls)),
    path('accounts/login/', GetAuthToken.as_view())
]