from django.shortcuts import render, redirect
from django.contrib import messages
from django.views.generic.edit import FormView
from django.contrib.auth import login, authenticate, logout, update_session_auth_hash
from django.urls import reverse_lazy
from accounts.forms import LoginForm,UserAdminCreationForm,UserAdminChangeForm,CustomeUserCreationForm,CustomeUserChangeForm,CustomeUserRegistrationForm
from accounts.models import User
from sevia_app.models import FormPatientDataV2,FormReviewerDataV1
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import PasswordChangeForm
from django.views import generic
from django.contrib.auth.models import Group
from sevia_app.models import Facility


from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse
from django.contrib.auth.forms import PasswordResetForm
from django.template.loader import render_to_string
from django.db.models.query_utils import Q
from django.utils.http import urlsafe_base64_encode
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.db.models import Count,Avg,F
import datetime
from django.http import JsonResponse
from django.shortcuts import get_object_or_404 



class LoginView(FormView):
    """ login view"""
    template_name = 'accounts/login.html'
    form_class = LoginForm
    success_url = reverse_lazy('sevia_app:home')

    def form_valid(self, form):
        post_data = form.cleaned_data
        user = authenticate(username=post_data['username'],
                            password=post_data['password'])
        if user is not None:
            login(self.request, user)
        else:
            messages.add_message(self.request, messages.INFO, 'Wrong credentials.\
                    Please try again with correct input')
            return redirect(reverse_lazy('accounts:login'))

        return redirect(reverse_lazy('sevia_app:home'))

def Logout(request):
    """logout logged in user"""
    logout(request)
    return redirect(reverse_lazy('accounts:login'))

def Maintainance(request):
    return render(request,'accounts/maintainance.html')

def password_reset_request(request):
	if request.method == "POST":
		password_reset_form = PasswordResetForm(request.POST)
		if password_reset_form.is_valid():
			data = password_reset_form.cleaned_data['email']
			associated_users = User.objects.filter(Q(email=data))
			if associated_users.exists():
				for user in associated_users:
					subject = "Password Reset Requested"
					email_template_name = "accounts/password_reset_email.txt"
					c = {
					"email":user.email,
					'domain':'sevia.esurveillance.or.tz',
					'site_name': 'SEVIA',
					"uid": urlsafe_base64_encode(force_bytes(user.pk)),
					"user": user,
					'token': default_token_generator.make_token(user),
					'protocol': 'http',
					}
					email = render_to_string(email_template_name, c)
					try:
						send_mail(subject, email, 'admin@example.com' , [user.email], fail_silently=False)
					except BadHeaderError:
						return HttpResponse('Invalid header found.')
					return redirect ("accounts:password_reset_done")
	password_reset_form = PasswordResetForm()
	return render(request=request, template_name="accounts/password_reset.html", context={"password_reset_form":password_reset_form})

@login_required
def profile(request):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('sevia_app:profile')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request,'sevia_app/profile.html',{'form':form})

class UserListView(LoginRequiredMixin, generic.ListView):
    template_name ='accounts/user_list.html'
    def get_queryset(self):
        user = self.request.user
        facilities_query = Facility.objects.filter(users=user)
        if user.is_admin:
            facilities_query = Facility.objects
        return User.objects.filter(facility__in=facilities_query.distinct()).distinct()

class UserCreateView(LoginRequiredMixin, generic.CreateView):   
    model = User
    form_class = CustomeUserCreationForm
    template_name ='accounts/user_create_form.html'
    success_url = reverse_lazy('accounts:user_create')

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super(UserCreateView, self).get_form_kwargs(**kwargs)
        form_kwargs["user"] = self.request.user
        return form_kwargs

class UserRegisterView(generic.CreateView):   
    model = User
    form_class = CustomeUserRegistrationForm
    template_name ='accounts/user_register_form.html'
    success_url = reverse_lazy('accounts:login')

class UserUpdateView(LoginRequiredMixin, generic.UpdateView):   
    model = User
    form_class = CustomeUserChangeForm
    template_name ='accounts/user_update_form.html'
    success_url = reverse_lazy('accounts:user_list')
    
    # def get_initial(self):
    #     initial = super(UserUpdateView, self).get_initial()
    #     initial.update({'groups': Group.objects.all(),})
    #     return initial

class UserDetailView(LoginRequiredMixin, generic.DetailView):   
    template_name ='accounts/user_detail.html'
    context_object_name = 'user_detail'
    queryset = User.objects.all()
    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)  
        patient_trend = FormPatientDataV2.objects.filter(provider=self.object).values_list('created_at__month').annotate(Count('uri'))
        my_list = []
        for x in range(1, 12):
            count = 0
            for item in patient_trend:
                if item[0] == x:
                    count = item[1]
            my_list.append(count)
        cases_count = self.object.get_total_cases()
        percentage_provider_accuracy = 0
        
        if cases_count > 0:
            provider_total_mismatch = FormReviewerDataV1.objects.filter(mismatch=0).filter(patient_data__provider=self.object).values_list('patient_data__provider').annotate(Count('mismatch'))    
            provider_total_mismatch = provider_total_mismatch[0][1] if provider_total_mismatch else 0;  
            percentage_provider_accuracy = provider_total_mismatch*100/cases_count

        average_response_minutes = FormReviewerDataV1.objects.filter(created_by=self.object).aggregate(average_time=Avg(F('created_at') - F('patient_data__created_at')))['average_time']
        average_response_minutes = average_response_minutes.total_seconds() if average_response_minutes else 0
        average_response_time_name = "Seconds"

        if average_response_minutes > 999:
            average_response_minutes = average_response_minutes /60
            average_response_time_name = "Minutes"

        if average_response_minutes > 999:
            average_response_minutes = average_response_minutes /60
            average_response_time_name = "Hours"
            
        data = {
            'facilities_count':self.object.facility_set.count(),
            'cases_count':cases_count,
            'case_trend_labels':["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            'case_trend_data':my_list,
            'first_role':self.object.groups.first().name,
            'percentage_provider_accuracy':percentage_provider_accuracy,
            'average_response_minutes': average_response_minutes,
            'average_response_time_name': average_response_time_name,
            }
        context['data'] = data
        return context             


class UserDeleteView(LoginRequiredMixin, generic.DeleteView):
    model = User
    template_name ='accounts/user_delete_form.html'
    success_url = reverse_lazy('accounts:user_list')






