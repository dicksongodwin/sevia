from django.contrib import admin
from tutorial.models import Tutorial,TutorialMediaFiles


class TutorialMediaFilesInlineAdmin(admin.TabularInline):
    model = TutorialMediaFiles 

class TutorialAdmin(admin.ModelAdmin):
    model =Tutorial
    list_display = ('id','creator','title','description','updated_at','created_on',)
    search_fields = ('id','title',)
    list_filter = ('title',)
    inlines = [TutorialMediaFilesInlineAdmin,]
    filter_horizontal = ()

class TutorialMediaFilesAdmin(admin.ModelAdmin):
    model =TutorialMediaFiles
    list_display = ('tutorial','tutorial_image','tutorial_file','video_url')
    search_fields = ('tutorial',)
    list_filter = ('tutorial',)
    filter_horizontal = ()

admin.site.register(Tutorial,TutorialAdmin)
admin.site.register(TutorialMediaFiles,TutorialMediaFilesAdmin)