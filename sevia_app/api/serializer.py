from sevia_app.models import Country,Region,District,Facility,Patient,FacilityGroupDetail,Form,FormData,FormPatientDataV1,FormReviewerDataV1,FormPatientDataV2
from rest_framework import serializers
from sevia_app.api.rabbit import do_send_event

class CountrySerializer(serializers.ModelSerializer):
    class Meta:
        model = Country
        fields ="__all__"

class RegionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Region
        fields ="__all__"

class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        model = District
        fields ="__all__"

class FacilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
        fields ="__all__"

class PatientSerializer(serializers.ModelSerializer):
    class Meta:
        model = Patient
        fields ="__all__"

class SimplifiedFacilitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Facility
        fields =fields = ('id','name','code','phone')

class FacilityGroupDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = FacilityGroupDetail
        fields ="__all__"

class FormSerializer(serializers.ModelSerializer):
    class Meta:
        model = Form
        fields ="__all__"

class FormDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormData
        fields ="__all__"
    
    def create(self, validated_data):
        obj = FormData.objects.create(**validated_data)
        obj.save()
        data = {"uri": str(obj.uri), "formId": str(obj.form_id.id), "createdBy": str(obj.created_by.id), "exported": obj.exported}
        res = do_send_event(data)
        return obj


class FormPatientDataV1Serializer(serializers.ModelSerializer):
    class Meta:
        model = FormPatientDataV1
        fields ="__all__"

class FormReviewerDataV1Serializer(serializers.ModelSerializer):
    class Meta:
        model = FormReviewerDataV1
        # fields =('uri','patient_data','created_at','created_by','comment')
        fields ="__all__"

class FormPatientDataV2Serializer(serializers.ModelSerializer):
    class Meta:
        model = FormPatientDataV2
        fields ="__all__"


