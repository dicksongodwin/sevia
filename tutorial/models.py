from django.db import models
from accounts.models import User


class Tutorial(models.Model):
    id = models.AutoField(primary_key=True)
    creator = models.ForeignKey(User,on_delete=models.PROTECT,)
    title = models.TextField(blank=True, null=True)
    description = models.TextField(blank=True, null=True)
    created_on = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def get_first_image(self):   
        if self.tutorialmediafiles_set.exclude(tutorial_image = None).count():
            return self.tutorialmediafiles_set.all()[0].tutorial_image

class TutorialMediaFiles(models.Model):
    tutorial = models.ForeignKey(Tutorial,on_delete=models.PROTECT,)
    tutorial_image = models.ImageField(upload_to='images/%Y/%m/%d',blank=True, null=True,default=None)
    tutorial_file = models.FileField(upload_to='tutorial_files/%Y/%m/%d',blank=True, null=True,default=None)
    video_url = models.TextField(blank=True, null=True)

    class Meta:
        verbose_name_plural = " Tutorial Media Files"

    def __str__(self):
        return str(self.tutorial)
