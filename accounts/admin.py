from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .forms import UserAdminCreationForm, UserAdminChangeForm
from .models import User
from django.contrib.auth.models import Permission

class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('id','username','fullname','phone','email', 'admin','updated_at')
    list_filter = ('admin',)
    fieldsets = (
        (None, {'fields': ('username', 'email','password')}),
        ('Personal info', {'fields': ('fullname','phone')}),
        ('Permissions', {'fields': ('active', 'staff', 'admin',
                                       'groups', 'user_permissions')}),
    )
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('username','fullname','email', 'password1','password2')}
        ),
    )
    search_fields = ('username','fullname')
    ordering = ('username','fullname')
    filter_horizontal = ()


admin.site.register(User, UserAdmin)
admin.site.register(Permission)
