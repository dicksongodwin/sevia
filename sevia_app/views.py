from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from sevia_app.models import Facility,Patient,Country,Region,District,FormPatientDataV1,FormPatientDataV2,FormReviewerDataV1
from sevia_app.forms import FormPatientDataCreationForm,FormPatientDataChangeForm,FormReviewerDataCreationForm   
from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.contrib.auth import get_user_model
from django.db.models import Count , Sum, Q
import datetime
from django.http import JsonResponse
from django.shortcuts import get_object_or_404 


User = get_user_model()

@login_required
def home(request):
    user = request.user
    facilities_query = Facility.objects.filter(users=user)
    if user.is_admin:
        facilities_query = Facility.objects
    patient_trend = FormPatientDataV2.objects.filter(patient__facility__in=facilities_query.distinct()).filter(created_at__year=datetime.datetime.now().year).values_list('created_at__month').annotate(Count('uri'))
    patient_trend_last_year = FormPatientDataV2.objects.filter(patient__facility__in=facilities_query.distinct()).filter(created_at__year="2016").values_list('created_at__month').annotate(Count('uri'))
    my_list = []
    prev_list = []

    for x in range(1, 12):
        count = 0
        for item in patient_trend:
            if item[0] == x:
                count = item[1]
        my_list.append(count) 
    for x in range(1, 12):
        for item in patient_trend_last_year:
            if item[0] == x:
                count = item[1]
        prev_list.append(count) 

    total_users = User.objects.filter(facility__in=facilities_query.distinct()).distinct().count()
    active_users = User.objects.filter(facility__in=facilities_query.distinct()).filter(last_login__year=datetime.datetime.now().year).distinct().count()
    inactive = total_users-active_users                
    data = {
        'users_count':User.objects.count(),
        'facilities_count':facilities_query.count(),
        'patient_count':Patient.objects.filter(facility__in=facilities_query.distinct()).distinct().count(),
        'cases_count':FormPatientDataV2.objects.filter(patient__facility__in=facilities_query.distinct()).count(),
        'case_trend_labels':["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        'case_trend_data': my_list,
        'prev_case_trend_data': prev_list,
        'active_user_trend':[inactive,active_users]
        }
    return render(request,'sevia_app/home.html',data)

@login_required
def users(request):
    return render(request,'sevia_app/users.html')


@login_required
def settings(request):
    return render(request,'sevia_app/settings.html')

def privacy_policy(request):
    return render(request,'sevia_app/privacy_policy.html')

def coming_soon(request):
    return render(request,'sevia_app/privacy_policy.html')

class ReportView(LoginRequiredMixin, generic.ListView):
    template_name ='sevia_app/reports.html'
    def get_queryset(self):
        user = self.request.user
        facilities_query = Facility.objects.filter(users=user)
        if user.is_admin:
            facilities_query = Facility.objects
        return FormPatientDataV2.objects.filter(patient__facility__in=facilities_query.distinct()).distinct()

class FacilityDetailView(LoginRequiredMixin, generic.DetailView):
    template_name ='sevia_app/facility_detail.html'
    context_object_name = 'facility'
    queryset = Facility.objects.all()
    def get_context_data(self, **kwargs):
        context = super(FacilityDetailView, self).get_context_data(**kwargs)
        patient_trend = Patient.objects.filter(created_at__year=datetime.datetime.now().year).values_list('created_at__month').annotate(Count('id'))
        my_list = []
        for x in range(1, 12):
            count = 0
            for item in patient_trend:
                if item[0] == x:
                    count = item[1]
            my_list.append(count)                  
        data = {
            'users_count':self.object.users.count(),
            'patient_count':Patient.objects.filter(facility = self.object).count(),
            'case_trend_labels':["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            'case_trend_data': my_list,
            }
        context['data'] = data
        return context


class FacilityListView(LoginRequiredMixin, generic.ListView):
    template_name ='sevia_app/facility_list.html'
    def get_queryset(self):
        return Facility.objects.filter(users=self.request.user).all()

class FacilityCreateView(LoginRequiredMixin, generic.CreateView):
    template_name ='sevia_app/facility_create_form.html'
    model = Facility
    fields = "__all__"
    success_url = reverse_lazy('sevia_app:facility_create')

class FacilityUpdateView(LoginRequiredMixin, generic.UpdateView):
    template_name ='sevia_app/facility_update_form.html'
    model = Facility
    fields = "__all__"
    success_url = reverse_lazy('sevia_app:facility_list')

class FacilityDeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name ='sevia_app/facility_delete_form.html'
    model = Facility
    success_url = reverse_lazy('sevia_app:facility_list')

class PatientDetailView(LoginRequiredMixin, generic.DetailView):
    template_name ='sevia_app/patient_detail.html'
    context_object_name = 'patient'
    queryset = Patient.objects.all()

class PatientListView(LoginRequiredMixin, generic.ListView):
    template_name ='sevia_app/patient_list.html'
    def get_queryset(self):
        user = self.request.user
        facilities_query = Facility.objects.filter(users=user)
        if user.is_admin:
            facilities_query = Facility.objects
        return Patient.objects.filter(facility__in=facilities_query.distinct()).distinct()

class PatientCreateView(LoginRequiredMixin, generic.CreateView):
    template_name ='sevia_app/patient_create_form.html'
    model = Patient
    fields = "__all__"
    success_url = reverse_lazy('sevia_app:patient_create')

class PatientUpdateView(LoginRequiredMixin, generic.UpdateView):
    template_name ='sevia_app/patient_update_form.html'
    model = Patient
    fields = "__all__"
    success_url = reverse_lazy('sevia_app:patient_list')

class PatientDeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name ='sevia_app/patient_delete_form.html'
    model = Patient
    success_url = reverse_lazy('sevia_app:patient_list')

class CountryListView(LoginRequiredMixin, generic.ListView):
    template_name ='sevia_app/country_list.html'
    def get_queryset(self):
        return Country.objects.all()

class CountryCreateView(LoginRequiredMixin, generic.CreateView):
    template_name ='sevia_app/country_create_form.html'
    model = Country
    fields = "__all__"
    success_url = reverse_lazy('sevia_app:country_create')

class CountryUpdateView(LoginRequiredMixin, generic.UpdateView):
    template_name ='sevia_app/country_update_form.html'
    model = Country
    fields = "__all__"
    success_url = reverse_lazy('sevia_app:country_list')

class CountryDeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name ='sevia_app/country_delete_form.html'
    model = Country
    success_url = reverse_lazy('sevia_app:country_list')

class RegionListView(LoginRequiredMixin, generic.ListView):
    template_name ='sevia_app/region_list.html'
    def get_queryset(self):
        return Region.objects.all()

class RegionCreateView(LoginRequiredMixin, generic.CreateView):
    template_name ='sevia_app/region_create_form.html'
    model = Region
    fields = "__all__"
    success_url = reverse_lazy('sevia_app:region_create')

class RegionUpdateView(LoginRequiredMixin, generic.UpdateView):
    template_name ='sevia_app/region_update_form.html'
    model = Region
    fields = "__all__"
    success_url = reverse_lazy('sevia_app:region_list')

class RegionDeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name ='sevia_app/region_delete_form.html'
    model = Region
    success_url = reverse_lazy('sevia_app:region_list')

class DistrictListView(LoginRequiredMixin, generic.ListView):
    template_name ='sevia_app/district_list.html'
    def get_queryset(self):
        return District.objects.all()

class DistrictCreateView(LoginRequiredMixin, generic.CreateView):
    template_name ='sevia_app/district_create_form.html'
    model = District
    fields = "__all__"
    success_url = reverse_lazy('sevia_app:district_create')

class DistrictUpdateView(LoginRequiredMixin, generic.UpdateView):
    template_name ='sevia_app/district_update_form.html'
    model = District
    fields = "__all__"
    success_url = reverse_lazy('sevia_app:district_list')

class DistrictDeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name ='sevia_app/district_delete_form.html'
    model = District
    success_url = reverse_lazy('sevia_app:district_list')

class PatientDataListView(LoginRequiredMixin, generic.ListView):
    paginate_by = 10
    template_name ='sevia_app/form_data/patient_data_v1_list.html'
    model = FormPatientDataV2
    def get_queryset(self):
        user = self.request.user
        facilities_query = Facility.objects.filter(users=user)
        if user.is_admin:
            object_list = self.model.objects.order_by('-updated_at')
            return object_list
        query = self.request.GET.get('q')
        if query:
            object_list = self.model.objects.order_by('-updated_at').filter(patient__facility__in=facilities_query.distinct()).filter(Q(patient__id__icontains=query)|Q(ccs_facility_no__icontains=query)|Q(provider__fullname__icontains=query)|Q(reviewer_data__created_by__fullname__icontains=query))
        else:
            object_list = self.model.objects.order_by('-updated_at').filter(patient__facility__in=facilities_query.distinct()).distinct()
        return object_list
    
class PatientDataCreateView(LoginRequiredMixin, generic.CreateView):
    template_name ='sevia_app/form_data/patient_data_v1_create_form.html'
    model = FormPatientDataV2
    form_class = FormPatientDataCreationForm
    success_url = reverse_lazy('sevia_app:patient_data_create')

    def form_valid(self, form):
        form.instance.provider = self.request.user
        return super().form_valid(form)


class PatientDataUpdateView(LoginRequiredMixin, generic.UpdateView):
    template_name ='sevia_app/form_data/patient_data_v1_update_form.html'
    model = FormPatientDataV2
    form_class = FormPatientDataChangeForm
    success_url = reverse_lazy('sevia_app:patient_data_list')
    def get_initial(self):
        initial = super().get_initial()

        hiv_status = self.get_object().hiv_status
        
        initial['hiv_status'] = hiv_status.capitalize() if hiv_status else ''
        initial['visit_status'] = self.get_object().visit_status
        initial['assessment'] = self.get_object().assessment
        return initial

class PatientDataDeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name ='sevia_app/form_data/patient_data_v1_delete_form.html'
    model = FormPatientDataV2
    success_url = reverse_lazy('sevia_app:patient_data_list')

class ReviewerDataListView(LoginRequiredMixin, generic.ListView):
    paginate_by = 10
    template_name ='sevia_app/form_data/reviewer_data_v1_list.html'
    model = FormReviewerDataV1
    def get_queryset(self):
        user = self.request.user
        facilities_query = Facility.objects.filter(users=user)
        if user.is_admin:
            object_list = self.model.objects.order_by('-created_at')
            return object_list
        query = self.request.GET.get('q')
        if query:
            object_list = self.model.objects.order_by('-created_at').filter(patient_data__patient__facility__in=facilities_query.distinct()).filter(Q(patient_data__uri__icontains=query)|Q(created_by__fullname__icontains=query)|Q(assessment__icontains=query)|Q(comment__icontains=query)).distinct()
        else:
            object_list = self.model.objects.order_by('-created_at').filter(patient_data__patient__facility__in=facilities_query.distinct()).distinct()
        return object_list


class ReviewerDataCreateView(LoginRequiredMixin, generic.CreateView):
    template_name ='sevia_app/form_data/reviewer_data_v1_create_form.html'
    model = FormReviewerDataV1
    form_class = FormReviewerDataCreationForm
    success_url = reverse_lazy('sevia_app:reviewer_data_list')

    def dispatch(self, request, *args, **kwargs):

        self.patient_data = get_object_or_404(FormPatientDataV2, pk=kwargs['patient_data_uri'])
        return super().dispatch(request, *args, **kwargs)

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        form.instance.patient_data = self.patient_data
        return super().form_valid(form)

class ReviewerDataUpdateView(LoginRequiredMixin, generic.UpdateView):
    template_name ='sevia_app/form_data/reviewer_data_v1_update_form.html'
    model = FormReviewerDataV1
    form_class = FormReviewerDataCreationForm
    success_url = reverse_lazy('sevia_app:reviewer_data_list')


class ReviewerDataDeleteView(LoginRequiredMixin, generic.DeleteView):
    template_name ='sevia_app/form_data/reviewer_data_v1_delete_form.html'
    model = FormReviewerDataV1
    success_url = reverse_lazy('sevia_app:reviewer_data_list')