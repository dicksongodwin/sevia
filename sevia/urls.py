from django.contrib import admin
from django.urls import path,include
from django.conf.urls.static import static 
from django.conf import settings
from sevia_app import views as sevia_views


admin.site.site_header = 'SEVIA Admin'
admin.site.index_title= 'SEVIA Admin Dashboard' 
urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('sevia_app.urls')),
    path('accounts/', include('accounts.urls')),
    path('tutorial/', include('tutorial.urls')),
    path('api-auth/', include('rest_framework.urls')),
    path('api/', include('accounts.api.urls')),
    path('api/sevia/', include('sevia_app.api.urls')),
    path('privacy-policy/', sevia_views.privacy_policy, name='privacy_policy'),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
