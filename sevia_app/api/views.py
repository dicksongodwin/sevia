from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.views import APIView
from sevia_app.api import serializer
from rest_framework.pagination import PageNumberPagination
from sevia_app.api.rabbit import do_send_event

from sevia_app.models import (
    Country,
    Region,
    District,
    Facility,
    FacilityGroupDetail,
    Form,
    FormData,
    FormPatientDataV1,
    FormReviewerDataV1,
    Patient,
    FormPatientDataV2,
    )
from accounts.models import (
    User
    )
from rest_framework.decorators import api_view, permission_classes, authentication_classes, renderer_classes
from rest_framework.permissions import IsAuthenticated
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from rest_framework.response import Response
from rest_framework.status import (
    HTTP_400_BAD_REQUEST,
    HTTP_404_NOT_FOUND,
    HTTP_200_OK
)
from sevia_app.db_helper import execute
from django.db.models import Count,Avg,F
from rest_framework.renderers import TemplateHTMLRenderer



class CountryViewSet(viewsets.ModelViewSet):

    queryset = Country.objects.all()
    serializer_class = serializer.CountrySerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

class RegionViewSet(viewsets.ModelViewSet):

    queryset = Region.objects.all()
    serializer_class = serializer.RegionSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

class DistrictViewSet(viewsets.ModelViewSet):

    queryset = District.objects.all()
    serializer_class = serializer.DistrictSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

class FacilityViewSet(viewsets.ModelViewSet):

    queryset = Facility.objects.all()
    serializer_class = serializer.FacilitySerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

class FacilityGroupDetailViewSet(viewsets.ModelViewSet):

    queryset = FacilityGroupDetail.objects.all()
    serializer_class = serializer.FacilityGroupDetailSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

class FormViewSet(viewsets.ModelViewSet):

    queryset = Form.objects.all()
    serializer_class = serializer.FormSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

class FormDataViewSet(viewsets.ModelViewSet):

    queryset = FormData.objects.all()
    serializer_class = serializer.FormDataSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

    # overide response for creating and updating
    def update(self, request, *args, **kwargs):
        super(FormDataViewSet, self).update(request, *args, **kwargs)
        return Response({"status": "Success"}) 
    
    def create(self, request, *args, **kwargs):
        ret = super(FormDataViewSet, self).create(request, *args, **kwargs)
        return Response({"status": "Success"}) 


class FormPatientDataV1ViewSet(viewsets.ModelViewSet):

    queryset = FormPatientDataV1.objects.all()
    serializer_class = serializer.FormPatientDataV1Serializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

class FormReviewerDataV1ViewSet(viewsets.ModelViewSet):

    queryset = FormReviewerDataV1.objects.all()
    serializer_class = serializer.FormReviewerDataV1Serializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

class PatientViewSet(viewsets.ModelViewSet):

    queryset = Patient.objects.all()
    serializer_class = serializer.PatientSerializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

class FormPatientDataV2ViewSet(viewsets.ModelViewSet):

    queryset = FormPatientDataV2.objects.all()
    serializer_class = serializer.FormPatientDataV2Serializer
    permission_classes = [IsAuthenticated]
    authentication_classes = [SessionAuthentication, TokenAuthentication]

@api_view(["GET",])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes((permissions.IsAuthenticated,))
def form_patient_data_unreviewed(request,*args,**kwargs):
    reviewed_patient_cases = FormReviewerDataV1.objects.values_list('patient_data', flat=True).distinct()
    data = FormPatientDataV1.objects.exclude(uri__in = reviewed_patient_cases).values()

    paginator = PageNumberPagination()
    paginator.page_size = 20

    result_page = paginator.paginate_queryset(data, request)
    # pagi = paginator.get_paginated_response(result_page)
    return Response(result_page, status=HTTP_200_OK)

@api_view(["GET",])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes((permissions.IsAuthenticated,))
def userforms(request,*args,**kwargs):
    user=User.objects.get(id=int(kwargs['pk']))
    user_countries = Country.objects.filter(region__district__facility__in=Facility.objects.filter(users=user).distinct())
    all_forms = Form.objects.filter(group_id__in=user.groups.all(),country_id__in=user_countries).order_by('-id').values()
    return Response(all_forms, status=HTTP_200_OK)

@api_view(["GET",])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes((permissions.IsAuthenticated,))
def get_provider_reviewed_cases(request,*args,**kwargs):
    id=int(kwargs['pk'])
    page_number = request.GET.get('page', 1)
    query = "select * from  fun_provider_get_reviewed_cases(%s, %s);"
    result = execute(query, [id, page_number],'default')
    return Response(result, status=HTTP_200_OK)

@api_view(["GET",])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes((permissions.IsAuthenticated,))
def get_provider_unreviewed_cases(request,*args,**kwargs):
    id=int(kwargs['pk'])
    page_number = request.GET.get('page', 1)
    query = "select * from  fun_provider_get_unreviewed_cases(%s, %s);"
    result = execute(query, [id, page_number],'default')
    return Response(result, status=HTTP_200_OK)

@api_view(["GET",])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes((permissions.IsAuthenticated,))
def get_provider_mismatch_cases(request,*args,**kwargs):
    id=int(kwargs['pk'])
    page_number = request.GET.get('page', 1)
    query = "select * from  fun_provider_get_mismatch_cases(%s, %s);"
    result = execute(query, [id, page_number],'default')
    return Response(result, status=HTTP_200_OK)

@api_view(["GET",])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes((permissions.IsAuthenticated,))
def get_reviewer_reviewed_cases(request,*args,**kwargs):
    id=int(kwargs['pk'])
    page_number = request.GET.get('page', 1)
    query = "select * from  fun_reviewer_get_reviewed_cases(%s, %s);"
    result = execute(query, [id, page_number],'default')
    return Response(result, status=HTTP_200_OK)

@api_view(["GET",])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes((permissions.IsAuthenticated,))
def get_reviewer_unreviewed_cases(request,*args,**kwargs):
    id=int(kwargs['pk'])
    page_number = request.GET.get('page', 1)
    query = "select * from  fun_reviewer_get_unreviewed_cases(%s, %s);"
    result = execute(query, [id, page_number],'default')
    return Response(result, status=HTTP_200_OK)

@api_view(["GET",])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes((permissions.IsAuthenticated,))
def get_reviewer_mismatch_cases(request,*args,**kwargs):
    id=int(kwargs['pk'])
    page_number = request.GET.get('page', 1)
    query = "select * from  fun_reviewer_get_mismatch_cases(%s, %s);"
    result = execute(query, [id, page_number],'default')
    return Response(result, status=HTTP_200_OK)

@api_view(["GET",])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes((permissions.IsAuthenticated,))
def get_case_data_json(request,*args,**kwargs):
    id=kwargs['pk']
    page_number = request.GET.get('page', 1)
    query = "select * from get_case_data_as_json(%s);"
    result = execute(query, [id],'default')
    return Response(result[0]['result'], status=HTTP_200_OK)

@api_view(["GET",])
@authentication_classes([SessionAuthentication, TokenAuthentication])
@permission_classes((permissions.IsAuthenticated,))
def test(request,*args,**kwargs):
    return Response("result", status=HTTP_200_OK)


@api_view(["GET",])
@permission_classes((permissions.AllowAny,))
@renderer_classes((TemplateHTMLRenderer,))
def app_dash(request,*args,**kwargs):
    id = request.GET.get('userId', None)
    user  = User.objects.get(id=id)
    patient_trend = FormPatientDataV2.objects.filter(provider=user).values_list('created_at__month').annotate(Count('uri'))
    my_list = []
    for x in range(1, 12):
        count = 0
        for item in patient_trend:
            if item[0] == x:
                count = item[1]
        my_list.append(count)
    cases_count = user.get_total_cases()
    percentage_provider_accuracy = 0
    
    if cases_count > 0:
        provider_total_mismatch = FormReviewerDataV1.objects.filter(mismatch=0).filter(patient_data__provider=user).values_list('patient_data__provider').annotate(Count('mismatch'))    
        provider_total_mismatch = provider_total_mismatch[0][1] if provider_total_mismatch else 0;  
        percentage_provider_accuracy = provider_total_mismatch*100/cases_count

    average_response_minutes = FormReviewerDataV1.objects.filter(created_by=user).aggregate(average_time=Avg(F('created_at') - F('patient_data__created_at')))['average_time']
    average_response_minutes = average_response_minutes.total_seconds() / 60 /60  if average_response_minutes else 0
    data = {
        'fullname':user.fullname,
        'roles':user.groups.all,
        'facilities_count':user.facility_set.count(),
        'facilities':user.facility_set.values('id','name'),
        'cases_count':cases_count,
        'case_trend_labels':["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        'case_trend_data':my_list,
        'first_role':user.groups.first().name,
        'percentage_provider_accuracy':percentage_provider_accuracy,
        'average_response_minutes': average_response_minutes,
        }

    return Response({'data': data}, template_name='sevia_app/user_app_dash.html')
    
