from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.views import APIView
from accounts.api.serializer import UserSerializer,GroupSerializer
from accounts.models import User
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.authtoken.models import Token
from rest_framework.response import Response
from sevia_app.api.serializer import SimplifiedFacilitySerializer,CountrySerializer
from sevia_app.models import Country,Facility
from rest_framework.permissions import IsAuthenticated


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-created_on')
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]


class GetAuthToken(ObtainAuthToken,APIView):

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data,
                                           context={'request': request})
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        groups_serializer = GroupSerializer(user.groups.all(), many=True)
        facility_serializer = SimplifiedFacilitySerializer(user.facility_set.all(), many=True)
        country_serializer = CountrySerializer(Country.objects.filter(region__district__facility__in=Facility.objects.filter(users=user).all()), many=True)
        return Response({
            'token': token.key,
            'user_id': user.id,
            'username': user.username,
            'fullname':user.fullname,
            'email': user.email,
            'phonenumber':user.phone,
            'active':user.active,
            'admin':user.admin,
            'created_on':user.created_on,
            'updated_at':user.updated_at,
            'roles':groups_serializer.data,
            'facilities':facility_serializer.data,
            'countries':country_serializer.data,
        })