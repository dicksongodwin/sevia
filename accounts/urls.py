from django.urls import path,include,reverse_lazy
from django.contrib.auth import views as auth_views
from . import views


app_name = 'accounts'
urlpatterns = [
    path('login/', views.LoginView.as_view(), name='login'),
    # path('login/', views.Maintainance, name='login'),
    path("password_reset/", views.password_reset_request, name="password_reset"),
    path('reset/<uidb64>/<token>/', auth_views.PasswordResetConfirmView.as_view(
    template_name="accounts/password_reset_confirm.html",success_url = reverse_lazy('accounts:password_reset_complete')), name='password_reset_confirm'),
    
    path('password_reset/done/', auth_views.PasswordResetDoneView.as_view(
    template_name='accounts/password_reset_done.html'), name='password_reset_done'),

    path('reset/done/', auth_views.PasswordResetCompleteView.as_view(
    template_name='accounts/password_reset_complete.html'), name='password_reset_complete'),

    path('logout/', views.Logout, name="logout"),
    path('register', views.UserRegisterView.as_view(), name='user_register'),
    path('users/', views.UserListView.as_view(), name='user_list'),
    path('user', views.UserCreateView.as_view(), name='user_create'),
    path('user/update/<int:pk>', views.UserUpdateView.as_view(), name='user_update'),
    path('user/detail/<int:pk>', views.UserDetailView.as_view(), name='user_detail'),
    path('user/delete/<int:pk>', views.UserDeleteView.as_view(), name='user_delete'),
    path('user/profile/', views.profile, name='profile'),
]
