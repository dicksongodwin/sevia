from django.urls import path
from tutorial import views

app_name = 'tutorial'
urlpatterns = [
    path('', views.TutorialListIndexView.as_view(), name='tutorial_index'),
    path('tutorial/view/<slug:pk>/', views.TutorialDetailView.as_view(), name='tutorial'),
    path('tutorials/', views.TutorialListView.as_view(), name='tutorial_list'),
    path('tutorial/', views.TutorialCreateView.as_view(), name='tutorial_create'),
    path('tutorial/update/<slug:pk>/', views.TutorialUpdateView.as_view(), name='tutorial_update'),
    path('tutorial/delete/<slug:pk>/', views.TutorialDeleteView.as_view(), name='tutorial_delete'),
]
