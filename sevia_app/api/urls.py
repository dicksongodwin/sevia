from django.urls import path,include
from sevia_app.api import views
from rest_framework import routers


router = routers.DefaultRouter()
router.register(r'country', views.CountryViewSet)
router.register(r'region', views.RegionViewSet)
router.register(r'district', views.DistrictViewSet)
router.register(r'facility', views.FacilityViewSet)
router.register(r'facility_group', views.FacilityGroupDetailViewSet)
router.register(r'form', views.FormViewSet)
router.register(r'form_data', views.FormDataViewSet)
router.register(r'form_patient_data', views.FormPatientDataV1ViewSet)
router.register(r'form_reviewer_data', views.FormReviewerDataV1ViewSet)
router.register(r'patient', views.PatientViewSet)
router.register(r'form_patient_data_v2', views.FormPatientDataV2ViewSet)

urlpatterns = [
    path('user/forms/<int:pk>/', views.userforms, name="user_forms"),
    path('form_patient_data/unreviewed/', views.form_patient_data_unreviewed, name="form_patient_data_unreviewed"),
    path('provider_reviewed/<int:pk>/', views.get_provider_reviewed_cases, name='provider_reviewed'),
    path('provider_unreviewed/<int:pk>/', views.get_provider_unreviewed_cases, name='provider_unreviewed'),
    path('provider_mismatch/<int:pk>/', views.get_provider_mismatch_cases, name='provider_mismatch'),
    path('reviewer_reviewed/<int:pk>/', views.get_reviewer_reviewed_cases, name='reviewer_reviewed'),
    path('reviewer_unreviewed/<int:pk>/', views.get_reviewer_unreviewed_cases, name='reviewer_unreviewed'),
    path('reviewer_mismatch/<int:pk>/', views.get_reviewer_mismatch_cases, name='reviewer_mismatch'),
    path('case_data_json/<str:pk>/', views.get_case_data_json, name='case_data_json'),
    path('test', views.test, name="test"),
    path('', include(router.urls)),
]