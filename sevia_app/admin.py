from django.contrib import admin
from .models import Facility,FacilityGroupDetail,Form,FormData,Country,Region,District,Patient,FormPatientDataV2,FormReviewerDataV1,DataDictionary



class FacilityAdmin(admin.ModelAdmin):
    model = Facility
    list_display = ('id','code','district','name','phone','updated_at','created_on')
    search_fields = ('id','code','district','name','phone',)
    # list_filter = ('code','name')
    filter_horizontal = ()

class FacilityGroupDetailAdmin(admin.ModelAdmin):
    model = FacilityGroupDetail
    list_display = ('id','group_name',)
    search_fields = ('id','group_name',)
    list_filter = ('group_name',)
    filter_horizontal = ()

class FormAdmin(admin.ModelAdmin):
    model = Form
    list_display = ('id','content','table_name','table_def','updated_at','created_on')
    search_fields = ('id','content','table_name','table_def',)
    list_filter = ('table_name',)
    filter_horizontal = ()

class FormDataAdmin(admin.ModelAdmin):
    model = FormData
    list_display = ('uri','created_by','exported','exported_at','data','updated_at','created_on')
    search_fields = ('uri','created_by','exported','exported_at',)
    list_filter = ('created_by','exported',)
    filter_horizontal = ()

class PatientAdmin(admin.ModelAdmin):
    model = Patient
    list_display = ('id','first_name','facility','updated_at')
    search_fields = ('first_name',)
    list_filter = ('first_name',)
    filter_horizontal = ()

class CountryAdmin(admin.ModelAdmin):
    model = Country
    list_display = ('id','name',)
    search_fields = ('name',)
    list_filter = ('name',)
    filter_horizontal = ()

class RegionAdmin(admin.ModelAdmin):
    model = Region
    list_display = ('id','name',)
    search_fields = ('name',)
    list_filter = ('name',)
    filter_horizontal = ()

class DistrictAdmin(admin.ModelAdmin):
    model = District
    list_display = ('id','name',)
    search_fields = ('name',)
    list_filter = ('name',)
    filter_horizontal = ()

# class RoleAdmin(admin.ModelAdmin):
#     model =Role
#     list_display = ('id','name','description','updated_at','created_on',)
#     search_fields = ('id','name',)
#     list_filter = ('name',)
#     filter_horizontal = ()

class PatientAdmin(admin.ModelAdmin):
    model =Patient
    list_display = ('id','first_name','phone','get_facility','updated_at',)
    search_fields = ('id','first_name',)
    list_filter = ('facility',)
    filter_horizontal = ()
    def get_facility(self, obj):
        return "\n".join([p.name for p in obj.facility.all()])


class FormPatientDataV2Admin(admin.ModelAdmin):
    model =FormPatientDataV2
    list_display = ('uri','visit_date','hiv_status','provider','phone','age','created_at')
    search_fields = ('provider__username',)
    list_filter = ('provider__username',)
    filter_horizontal = ()
    ordering = ['-created_at']

class FormReviewerDataV1Admin(admin.ModelAdmin):
    model =FormReviewerDataV1
    list_display = ('uri','patient_data','created_at','created_by','assessment','comment','mismatch')
    search_fields = ('uri','assessment',)
    list_filter = ('assessment','created_by__username',)
    filter_horizontal = ()

class DataDictionaryAdmin(admin.ModelAdmin):
    model =FormReviewerDataV1
    list_display = ('formId','formVersion','variableKey','variableName','description','status',)
    search_fields = ('formId',)
    list_filter = ('formId',)
    filter_horizontal = ()


admin.site.register(Facility,FacilityAdmin)
admin.site.register(FacilityGroupDetail,FacilityGroupDetailAdmin)
admin.site.register(Form,FormAdmin)
admin.site.register(FormData,FormDataAdmin)
admin.site.register(Country,CountryAdmin)
admin.site.register(Region,RegionAdmin)
admin.site.register(District,DistrictAdmin)
# admin.site.register(Role,RoleAdmin)
admin.site.register(Patient,PatientAdmin)
admin.site.register(FormPatientDataV2,FormPatientDataV2Admin)
admin.site.register(FormReviewerDataV1,FormReviewerDataV1Admin)
admin.site.register(DataDictionary,DataDictionaryAdmin)